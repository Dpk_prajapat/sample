class CreateCars < ActiveRecord::Migration[6.0]
  def change
    create_table :cars do |t|
      t.string :price 
      t.string :name
      t.string :type
      t.string :driver_name
      t.timestamps
    end
  end
end
